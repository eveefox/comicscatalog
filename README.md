# README #

This appliaction is made to overviewing and searching Marvel comics.
Data are downloaded using [https://developer.marvel.com](https://developer.marvel.com) API

### What is this repository for? ###

* Catalog od Marvel's comics (need internet connection)
* 1.0

### How to run project? ###

* download repository from master branch
* run MarvelComics.xcworkspace
* repository has Reveal pod included, but if anything is not running properly run `pod install`
* pod is not necessary for proper working of project. It helps in setting auto layout

### Future improvements ###

* setting timeout and more alerts about wrong connection
* better autolayout for using on horizontal screen
* prettier UI implementation
