//
//  ComicsListTableViewController.swift
//  MarvelComics
//
//  Created by Ewelina on 29/01/2022.
//

import UIKit

class ComicsListTableViewController: TBViewController {
    //private var comicsListVM: ComicsListViewModel!
    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }
    
    private func setup() {
        self.navigationController?.navigationBar.prefersLargeTitles = true
        
        tableView.register(ComicsCellView.self, forCellReuseIdentifier: "comicsCell")
        Webservice().getComics() { comics in
            
            if let comics = comics {
                
                self.setupData(data: ComicsListViewModel(comics))
            }
        }
    }
    
    
}


class TBViewController: UITableViewController {
    private var comicsListVM: ComicsListViewModel!
    
    func setupData(data:ComicsListViewModel){
        comicsListVM = data
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    func setEmptyMessage(_ message: String) {
        let messageLabel = UILabel(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        messageLabel.text = message
        messageLabel.textColor = .black
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = .systemFont(ofSize: 18)
        messageLabel.sizeToFit()
        
        tableView.backgroundView = messageLabel
        tableView.separatorStyle = .none
    }
    
    func restore() {
        tableView.backgroundView = nil
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        let things = self.comicsListVM.numberOfRowsInSection(section)
        if things == 0 {
            self.setEmptyMessage("No comics found")
        } else {
            self.restore()
        }
        
        return things
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return self.comicsListVM == nil ? 0 : self.comicsListVM.numberOfSections
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "comicsCell", for: indexPath) as? ComicsCellView else {
            fatalError("Comics cell not found")
        }
        
        let comicsVM = self.comicsListVM.comicsAtIndex(indexPath.row)
        
        cell.comics = comicsVM
        comicsVM.getCover(cell.comicaImageView)
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let comicsToSeeDetails = self.comicsListVM.comicsAtIndex(indexPath.row)
        
        let detailsVC = ComicsDetailViewController(comicsToSeeDetails)
        
        navigationController?.pushViewController(detailsVC, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 200
    }
}
