//
//  SearchComicsViewController.swift
//  MarvelComics
//
//  Created by Ewelina on 04/02/2022.
//

import UIKit


class SearchComicsViewController: TBViewController, UISearchResultsUpdating {
    private var searchController: UISearchController!
    var comicsListVM: ComicsListViewModel = ComicsListViewModel(nil)
    let activitiIndic = CutomActivityIndicator()
    let activityView = UIView()
    
    private enum ExpressionKeys: String {
      case title
    }
    
    // MARK: - View Life Cycle
    override func viewDidLoad() {
      super.viewDidLoad()
      setup()
    
    }
    private func setupActivityIndicator(){
        //activitiIndic.frame = CGRect(x: view.center.x, y: view.center.y-40, width: 30, height: 30)
        if activityView.subviews.count == 0 {
            activityView.addSubview(activitiIndic)
            activitiIndic.center = view.center
            activitiIndic.animate()
        }
        
        tableView.backgroundView = activityView
        tableView.separatorStyle = .none
        
    }
    
    func setSearchMessage() {
        let noSearchView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height))
        
        let messageLabel = UILabel()
            messageLabel.text = "Start typing to find a particular comics."
            messageLabel.textColor = .black
            messageLabel.numberOfLines = 0
            messageLabel.textAlignment = .center
            messageLabel.font = .systemFont(ofSize: 18)
            messageLabel.sizeToFit()
            
            let bookImage = UIImageView(image: UIImage(systemName: "book.fill"))
            bookImage.frame = CGRect(x: 0, y: 0, width: 100, height: 70)
            bookImage.tintColor = .lightGray
            bookImage.alpha = 0.5
            noSearchView.addSubview(bookImage)
            noSearchView.addSubview(messageLabel)
            bookImage.center = noSearchView.center
            messageLabel.center = CGPoint(x: noSearchView.center.x, y: bookImage.center.y + 60)
            
            tableView.backgroundView = noSearchView
            tableView.separatorStyle = .none
        }

        func restoreSearch() {
            tableView.backgroundView = nil
            
        }
    
    func setup() {
        searchController = UISearchController()
        searchController.searchResultsUpdater = self
        searchController.searchBar.placeholder = "Search for the comic book"
        searchController.searchBar.autocapitalizationType = .none
        searchController.obscuresBackgroundDuringPresentation = false // The default is true.
        setSearchMessage()
        navigationItem.searchController = searchController
        navigationItem.hidesSearchBarWhenScrolling = false // Make the search bar always visible.
    }
    
    
    func updateSearchResults(for searchController: UISearchController) {
        if !searchController.isActive {
            self.setupData(data: ComicsListViewModel([]))
            
        } else {
        setupActivityIndicator()
        tableView.register(ComicsCellView.self, forCellReuseIdentifier: "comicsCell")
        guard let searchText = searchController.searchBar.text else {
                return
            }
            print(searchText)
            if searchText.count > 1 {
                Webservice().searchComics(phrase: searchText ) { comics in
                    if let comics = comics {
                        self.setupData(data: ComicsListViewModel(comics))
                    }
                    
                }
            } else {
                restoreSearch()
            }
        }
        
    }
    

}


