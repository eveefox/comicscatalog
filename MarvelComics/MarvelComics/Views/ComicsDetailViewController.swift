//
//  ComicsDetailViewController.swift
//  MarvelComics
//
//  Created by Ewelina on 01/02/2022.
//

import UIKit

class ComicsDetailViewController: UIViewController {
    let comics: Comics?
    let detailsUrl: URL?
    var previousUrlString: String?
    
    var bigCover: UIImageView = {
             let img = UIImageView()
             img.contentMode = .scaleAspectFill
             img.translatesAutoresizingMaskIntoConstraints = false
             img.clipsToBounds = true
            return img
        }()
    
    let findOutButton: UIButton = {
        var filled = UIButton.Configuration.filled()
        filled.title = "Find out more"
        filled.buttonSize = .large
        filled.baseBackgroundColor = .systemRed
        return UIButton(configuration: filled, primaryAction: nil)
    }()
    
    lazy var swipeView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 20
        view.clipsToBounds = true
        return view
    }()
    
    let titleLabel:UILabel = {
            let label = UILabel()
            label.font = UIFont.boldSystemFont(ofSize: 18)
            label.textColor =  .black
            label.numberOfLines = 0
            label.translatesAutoresizingMaskIntoConstraints = false
            return label
    }()
    
    let creatorsLabel:UILabel = {
      let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textColor =  .lightGray
        label.clipsToBounds = true
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let descriptionLabel:UILabel = {
      let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor =  .black
        label.clipsToBounds = true
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let stackView:UIStackView = {
      let SV = UIStackView()
        SV.translatesAutoresizingMaskIntoConstraints = false
        SV.clipsToBounds = true
        SV.axis  = NSLayoutConstraint.Axis.vertical
        SV.distribution  = UIStackView.Distribution.fill
        SV.alignment = UIStackView.Alignment.leading
        SV.spacing = 8
        SV.alignment = .top
      return SV
    }()
//MARK: - Swipe View Base Constants
    let defaultHeight: CGFloat = 300
    let dismissibleHeight: CGFloat = 50
    let maximumContainerHeight: CGFloat = UIScreen.main.bounds.height - 200
    var currentContainerHeight: CGFloat = 300
    
    var containerViewHeightConstraint: NSLayoutConstraint?
    var containerViewBottomConstraint: NSLayoutConstraint?
    
//MARK: - Init and load
    init(_ comicsVM: ComicsViewModel) {
        self.comics = comicsVM.comicsItem
        comicsVM.getCover(bigCover)
        
        titleLabel.text = comicsVM.title
        descriptionLabel.text = comicsVM.desc
        creatorsLabel.text = comicsVM.creators
        self.detailsUrl = comicsVM.detailsUrl
        
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationAndTabBar()
        setupConstraints()
        setupPanGesture()
        findOutButton.addTarget(self, action: #selector(openLink), for: .touchUpInside)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        animatePresentContainer()
    }
    
    private func navigationAndTabBar() {
        navigationItem.title = "Details"
        navigationItem.largeTitleDisplayMode = .never
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(
            image: UIImage(systemName: "arrow.backward"),
            style: .plain,
            target: self,
            action: #selector(goBack)
        )
        navigationItem.leftBarButtonItem?.tintColor = .black
    }
    
    func setupConstraints() {
        view.backgroundColor = .white
        view.addSubview(bigCover)
        bigCover.translatesAutoresizingMaskIntoConstraints = false
        
        
        let safeArea = view.safeAreaLayoutGuide
        NSLayoutConstraint.activate([
            bigCover.topAnchor.constraint(equalTo: safeArea.topAnchor, constant: 0),
            bigCover.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: 0),
            bigCover.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 0),
            bigCover.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: 0)

        ])
        
        
        
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(creatorsLabel)
        stackView.addArrangedSubview(descriptionLabel)
        
        swipeView.addSubview(stackView)
        view.addSubview(swipeView)
        view.addSubview(findOutButton)
        findOutButton.translatesAutoresizingMaskIntoConstraints = false
        swipeView.translatesAutoresizingMaskIntoConstraints = false
        
        //stackview with title, creators, desc
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: swipeView.leadingAnchor, constant: 20),
            stackView.topAnchor.constraint(equalTo: swipeView.topAnchor, constant: 20),
            stackView.trailingAnchor.constraint(equalTo: swipeView.trailingAnchor, constant: -20)
        ])
        
        NSLayoutConstraint.activate([
            swipeView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            swipeView.trailingAnchor.constraint(equalTo: view.trailingAnchor)
        ])
        
        NSLayoutConstraint.activate([
            findOutButton.bottomAnchor.constraint(equalTo: safeArea.bottomAnchor, constant: -20),
            findOutButton.leadingAnchor.constraint(equalTo: safeArea.leadingAnchor, constant: 30),
            findOutButton.trailingAnchor.constraint(equalTo: safeArea.trailingAnchor, constant: -30)

        ])
        
        containerViewHeightConstraint = swipeView.heightAnchor.constraint(equalToConstant: defaultHeight)
        containerViewBottomConstraint = swipeView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: defaultHeight)
        containerViewHeightConstraint?.isActive = true
        containerViewBottomConstraint?.isActive = true
    }
//MARK: - setup dragging gesture
    func setupPanGesture() {
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.handlePanGesture(gesture:)))
        panGesture.delaysTouchesBegan = false
        panGesture.delaysTouchesEnded = false
        view.addGestureRecognizer(panGesture)
    }
    
// MARK: - handle dragging gesture
    @objc func handlePanGesture(gesture: UIPanGestureRecognizer) {
        let translation = gesture.translation(in: view)
        let isDraggingDown = translation.y > 0
        let newHeight = currentContainerHeight - translation.y
        
        switch gesture.state {
        case .changed:
            if newHeight < maximumContainerHeight {
                containerViewHeightConstraint?.constant = newHeight
                view.layoutIfNeeded()
            }
        case .ended:
            
            if newHeight < defaultHeight {
                animateContainerHeight(defaultHeight)
            }
            else if newHeight < maximumContainerHeight && isDraggingDown {
                animateContainerHeight(defaultHeight)
            }
            else if newHeight > defaultHeight && !isDraggingDown {
                animateContainerHeight(maximumContainerHeight)
            }
        default:
            break
        }
    }
//MARK: - dragging animations configuration
    func animateContainerHeight(_ height: CGFloat) {
        UIView.animate(withDuration: 0.4) {
            self.containerViewHeightConstraint?.constant = height
            self.view.layoutIfNeeded()
        }
        currentContainerHeight = height
    }
    
// MARK: - present animation
    func animatePresentContainer() {
        UIView.animate(withDuration: 0.3) {
            self.containerViewBottomConstraint?.constant = 0
            self.view.layoutIfNeeded()
        }
    }
//MARK: - dismiss view function
    @objc private func openLink() {
        if detailsUrl != nil {
            UIApplication.shared.open(detailsUrl!)
        }
    }
    @objc private func goBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    deinit{
        print("check deinited")
    }
}



