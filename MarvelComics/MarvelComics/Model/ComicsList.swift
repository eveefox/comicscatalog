//
//  ComicsList.swift
//  MarvelComics
//
//  Created by Ewelina on 29/01/2022.
//

import Foundation

struct Response: Decodable {  var data: ComicsList }
struct ComicsList: Decodable{ var results: [Comics] }

