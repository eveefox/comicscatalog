//
//  Comics.swift
//  MarvelComics
//
//  Created by Ewelina on 29/01/2022.
//

import Foundation
import UIKit
/*
 Comics:
    title: String
    description: String
    urls:
        url: String
    thumbnail:
        exten: String
        path: String
    creators (CreatorList):
        items (Creator): [
            name: String
            ]
 */

struct Comics: Decodable {
    var title: String?
    var description: String?
    let urls: [Urls]?
    let cover: Cover?
    let creators: Creators?
    
   //match property names
   enum CodingKeys: String, CodingKey {
       case title
       case description
       case urls
       case cover = "thumbnail"
       case creators
   }
    
    
}
struct Cover: Decodable {
    let imageExtension: String?
    let path: String?
    var coverUrl: String?
    
    enum CodingKeys: String, CodingKey {
        case imageExtension = "extension"
        case path
    }
    //add full url as additional variable
    init(from decoder: Decoder) throws {
            let container = try decoder.container(keyedBy: CodingKeys.self)
            imageExtension = try container.decode(String.self, forKey: .imageExtension)
            path = try container.decode(String.self, forKey: .path)
            let http = path!+"."+imageExtension!
            coverUrl = "https" + http.dropFirst(4)
    }
    
}
 struct Urls: Decodable { var url: URL?}
 struct Creator: Decodable { let name: String }
 struct Creators: Decodable { let items: [Creator]? }
