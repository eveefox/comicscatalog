//
//  CutomActivityIndicator.swift
//  MarvelComics
//
//  Created by Ewelina on 05/02/2022.
//

import UIKit

class CutomActivityIndicator: UIView {

    let activityInd = CAShapeLayer()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        configure()
    }
    required init?(coder: NSCoder) {
        fatalError("init coder has not been implemented")
    }
    
    private func configure() {
        frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        let rect = self.bounds
        let circularPath = UIBezierPath(ovalIn: rect)
        
        activityInd.path = circularPath.cgPath
        activityInd.fillColor = UIColor.clear.cgColor
        activityInd.strokeColor = UIColor.systemRed.cgColor
        activityInd.lineWidth = 7
        activityInd.strokeEnd = 0.75
        activityInd.lineCap = .round
        
        self.layer.addSublayer(activityInd)
    }
    
    func animate() {
        UIView.animate(withDuration: 1, delay: 0, options: .curveLinear) {
            self.transform = CGAffineTransform(rotationAngle: .pi)
        } completion: { (complected) in
            UIView.animate(withDuration: 1, delay: 0, options: .curveLinear) {
                self.transform = CGAffineTransform(rotationAngle: 0)
            } completion: { (completed) in
                self.animate()
            }
        }
        
    }

}
