//
//  ComicsCellView.swift
//  MarvelComics
//
//  Created by Ewelina on 30/01/2022.
//

import UIKit

class ComicsCellView: UITableViewCell {
    var previousUrlString: String?

    let comicaImageView:UIImageView = {
             let img = UIImageView()
             img.contentMode = .scaleAspectFill // image will never be strecthed vertially or horizontally
             img.translatesAutoresizingMaskIntoConstraints = false // enable autolayout
             img.layer.cornerRadius = 10
             img.clipsToBounds = true
            return img
         }()
    
    let titleLabel:UILabel = {
            let label = UILabel()
            label.font = UIFont.boldSystemFont(ofSize: 18)
            label.textColor =  .black
            label.translatesAutoresizingMaskIntoConstraints = false
            return label
    }()
    
    let creatorsLabel:UILabel = {
      let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 12)
        label.textColor =  .lightGray
        label.clipsToBounds = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let descriptionLabel:UILabel = {
      let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.textColor =  .black
        label.clipsToBounds = true
        label.numberOfLines = 0
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let stackView:UIStackView = {
      let SV = UIStackView()
        SV.translatesAutoresizingMaskIntoConstraints = false
        SV.clipsToBounds = true // this will make sure its children do not go out of the boundary
        SV.axis  = NSLayoutConstraint.Axis.vertical
        SV.distribution  = UIStackView.Distribution.fill
        SV.alignment = UIStackView.Alignment.leading
        SV.spacing = 8
        SV.alignment = .top
      return SV
    }()
    
    let containerView:UIView = {
      let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true // this will make sure its children do not go out of the boundary
        view.layer.cornerRadius = 10
        view.backgroundColor = .white
      return view
    }()
    
    var comics: ComicsViewModel? {
        didSet {
            titleLabel.text = comics?.title
            descriptionLabel.text = comics?.desc
            creatorsLabel.text = comics?.creators
        }
    }
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        stackView.addArrangedSubview(titleLabel)
        stackView.addArrangedSubview(creatorsLabel)
        stackView.addArrangedSubview(descriptionLabel)
        
        containerView.addSubview(comicaImageView)
        containerView.addSubview(stackView)
        
        self.contentView.addSubview(containerView)
        
        self.contentView.backgroundColor = UIColor(red: 0.965, green: 0.965, blue: 0.965, alpha: 1)
        
        //cover image
        NSLayoutConstraint.activate([
            comicaImageView.leadingAnchor.constraint(equalTo:containerView.leadingAnchor, constant:0),
            comicaImageView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 0),
            comicaImageView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: 0),
            comicaImageView.widthAnchor.constraint(equalToConstant:120)
        ])
        
        //stackview with title, creators, desc
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: comicaImageView.trailingAnchor, constant: 15),
            stackView.trailingAnchor.constraint(equalTo: containerView.trailingAnchor, constant: -15),
            stackView.topAnchor.constraint(equalTo: containerView.topAnchor, constant: 15),
            stackView.bottomAnchor.constraint(equalTo: containerView.bottomAnchor, constant: -15)
            
        ])
        stackView.contentMode = .topLeft
        
        NSLayoutConstraint.activate([
            containerView.centerYAnchor.constraint(equalTo:self.contentView.centerYAnchor),
            containerView.centerXAnchor.constraint(equalTo:self.contentView.centerXAnchor),
            containerView.widthAnchor.constraint(equalTo:self.contentView.widthAnchor, constant: -30),
            containerView.heightAnchor.constraint(equalTo:self.contentView.heightAnchor, constant: -20)
            
        ])
        
        titleLabel.setContentHuggingPriority(UILayoutPriority(300), for: .vertical)
        creatorsLabel.setContentHuggingPriority(UILayoutPriority(301), for: .vertical)
        descriptionLabel.setContentHuggingPriority(UILayoutPriority(260), for: .vertical)
        
        titleLabel.setContentCompressionResistancePriority(UILayoutPriority(802), for: .vertical)
        creatorsLabel.setContentCompressionResistancePriority(UILayoutPriority(801), for: .vertical)
        descriptionLabel.setContentCompressionResistancePriority(UILayoutPriority(800), for: .vertical)

    }

     required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }
      
    
    func apply(with urlString: String){
        
            CoverDownloader.shared.downloadImage(with: urlString, completionHandler: { (image, cached) in
                if cached || (urlString == self.previousUrlString) {
                    self.comicaImageView.image = image
                }

            }, placeholderImage: UIImage(named: "placeholder"))
        
        previousUrlString = urlString

    }
    
}
