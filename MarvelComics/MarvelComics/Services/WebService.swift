//
//  WebService.swift
//  MarvelComics
//
//  Created by Ewelina on 29/01/2022.
//

import UIKit

class Webservice {

    private let marvelAPIUrl = "https://gateway.marvel.com/v1/public/comics?ts=1&apikey=080a502746c8a60aeab043387a56eef0&hash=6edc18ab1a954d230c1f03c590d469d2&limit=25&offset=0&orderBy=-onsaleDate"
    
    func getComics(completion: @escaping ([Comics]?) -> ()) {
        guard let url = URL(string: marvelAPIUrl+"&limit=25") else { return }
        
        URLSession.shared.dataTask(with: url) { (d, repsonse, error) in
            
            if let error = error {
                completion(nil)
            } else if let data = d {
                let root = try? JSONDecoder().decode(Response.self, from: data)
                
                if let comicsList = root?.data.results{
                    completion(comicsList)
                }
                
            }
        }.resume()
    }
    func searchComics(phrase:String, completion: @escaping ([Comics]?) -> ()) {
        guard let urlString = String(marvelAPIUrl+"&title=\(phrase)").addingPercentEncoding(withAllowedCharacters: CharacterSet.urlQueryAllowed) else { return }
        
        guard let url = URL(string: urlString) else { return }
        
        URLSession.shared.dataTask(with: url) { (d, repsonse, error) in
            
            if let error = error {
                print(error.localizedDescription)
                completion(nil)
                
            } else if let data = d {
                let root = try? JSONDecoder().decode(Response.self, from: data)
                
                if let comicsList = root?.data.results{
                    completion(comicsList)
                }
                
            }
        }.resume()
        
        
        
    }
    
}

class CoverDownloader {
    
    static let shared = CoverDownloader()
    
    private var cachedImages: [String: UIImage]
    private var imagesDownloadTasks: [String: URLSessionDataTask]
    
    // MARK: Private init
    private init() {
        cachedImages = [:]
        imagesDownloadTasks = [:]
    }
    
    // A serial queue to be able to write the non-thread-safe dictionary
    let serialQueueForImages = DispatchQueue(label: "images.queue", attributes: .concurrent)
    let serialQueueForDataTasks = DispatchQueue(label: "dataTasks.queue", attributes: .concurrent)
    
    func downloadImage(with imageUrlString: String?,
                       completionHandler: @escaping (UIImage?, Bool) -> Void,
                       placeholderImage: UIImage?) {
        
        guard let imageUrlString = imageUrlString else {
            completionHandler(placeholderImage, true)
            return
        }
        
        if let image = getCachedImageFrom(urlString: imageUrlString) {
            completionHandler(image, true)
        } else {
            guard let url = URL(string: imageUrlString) else {
                completionHandler(placeholderImage, true)
                return
            }
            
            if let _ = getDataTaskFrom(urlString: imageUrlString) {
                return
            }
            
            let task = URLSession.shared.dataTask(with: url) {(data, response, error) in
                
                guard let data = data else { return }
                
                if let _ = error {
                    DispatchQueue.main.async {
                        completionHandler(placeholderImage, true)
                    }
                    return
                }
                
                let image = UIImage(data: data)
                
                self.serialQueueForImages.sync(flags: .barrier) {
                    self.cachedImages[imageUrlString] = image
                }
                
                _ = self.serialQueueForDataTasks.sync(flags: .barrier) {
                    self.imagesDownloadTasks.removeValue(forKey: imageUrlString)
                }
                
                DispatchQueue.main.async {
                    completionHandler(image, false)
                }
            }
            
            self.serialQueueForDataTasks.sync(flags: .barrier) {
                imagesDownloadTasks[imageUrlString] = task
            }
            
            task.resume()
        }
    }
    
    
    private func getCachedImageFrom(urlString: String) -> UIImage? {
        serialQueueForImages.sync {
            return cachedImages[urlString]
        }
    }
    
    private func getDataTaskFrom(urlString: String) -> URLSessionTask? {
        serialQueueForDataTasks.sync {
            return imagesDownloadTasks[urlString]
        }
    }
}
