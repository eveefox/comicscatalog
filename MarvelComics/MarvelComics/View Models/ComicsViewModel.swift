//
//  ComicsViewModel.swift
//  MarvelComics
//
//  Created by Ewelina on 30/01/2022.
//

import Foundation
import UIKit

class ComicsListViewModel{
    
    let comicsList: [Comics]
    
    init(_ comicsList: [Comics]?){
        self.comicsList = comicsList ?? []
    }
    
    var numberOfSections: Int {
        return 1
    }
    func numberOfRowsInSection(_ section: Int) -> Int {
        return self.comicsList.count
        
    }
    func comicsAtIndex(_ index: Int) -> ComicsViewModel {
        let comics = self.comicsList[index]
        return ComicsViewModel(comics)
    }
}


class ComicsViewModel{
    private var previousUrlString: String?
    var comicsItem: Comics?
    var title: String?
    var creators: String
    var desc: String
    var detailsUrl: URL?
    
    init(_ comics: Comics) {
        self.comicsItem = comics
        self.title = comics.title
        
        self.detailsUrl = comics.urls?.first?.url ?? nil
        if let creators = comics.creators?.items {
            let creatorsList = creators.map(){ String($0.name) }
            if creatorsList.count>0{
                self.creators = "written by \(creatorsList.joined(separator: ", "))"
            } else {
                self.creators = "no data"
            }
        } else {
            self.creators = "no data"
        }
        
        if let desc = comics.description {
            self.desc = desc
        } else {
            self.desc = ""
        }
        
    }
    
    func getCover(_ sender: UIImageView) {
        guard let url = self.comicsItem?.cover?.coverUrl else {return }
        CoverDownloader.shared.downloadImage(with: url, completionHandler: { (image, cached) in
                if cached || (url == self.previousUrlString) {
                    sender.image = image
                }
            }, placeholderImage: UIImage(named: "placeholder"))
        
        previousUrlString = url
    }
    
    
}

